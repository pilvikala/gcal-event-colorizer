// this script colorizes events with guests from outside of your company with the specified color so you can clearly see them in your calendar

// events where only people with the following domain in the email are present will not be colorized
const InternalDomain = "example.com";
// color to use for meetings with external guests
const Color = CalendarApp.EventColor.GREEN;

function ColorEvents() {

  function isExternalEmail(email) {
    if(!email) {
      return false;
    }
    return !email.endsWith(InternalDomain) && !email.endsWith("@resource.calendar.google.com");
  }

  const today = new Date();
  let inTwoWeeks = new Date();
  inTwoWeeks.setDate(inTwoWeeks.getDate() + 14);
  var calendars = CalendarApp.getAllOwnedCalendars();

  calendars.forEach((cal)=>{
    const events = cal.getEvents(today, inTwoWeeks);
    events.forEach((event)=>{
      const guestList = event.getGuestList();
      const index = guestList.findIndex((guest)=>isExternalEmail(guest.getEmail()));
      if(index > -1) {
        event.setColor(Color);
      }
    });
  });
}
